package edu.ucsp.is.learn;

public class Node {
	private int data;
    private Node[] child;

    Node(int dat) {
        data = dat;
        child = new Node[2];
    }

    public int getData() {
        return data;
    }
    public Node getChild(int id) {
        return child[id];
    }

    public void setChild(int id, Node child) {
        this.child[id] = child;
    }

}

