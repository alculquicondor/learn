package edu.ucsp.is.learn;

public class Tree {
    Node root;

    public boolean root_exist() {
        return root != null;
    }

    public boolean add(int data) {
        if (!root_exist()) {
            root = new Node(data);
            return true;
        }
        Node prev, curr = root;
        int left;
        do {
            if (curr.getData() == data)
                return false;
            left = curr.getData() < data ? 1 : 0;
            prev = curr;
            curr = curr.getChild(left);
        } while (curr != null);
        prev.setChild(left, new Node(data));
        return true;
    }

    public static void main(String[] args) {
        Tree t = new Tree();
        assert t.add(5) == true;
        assert t.add(5) == false;
        assert t.add(8) == true;
        assert t.add(7) == true;
        assert t.add(4) == true;
        assert t.add(6) == true;
        assert t.add(4) == false;
        System.out.println("OK");
    }
}

